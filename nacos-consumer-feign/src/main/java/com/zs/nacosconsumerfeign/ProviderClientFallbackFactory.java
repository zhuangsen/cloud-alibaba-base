package com.zs.nacosconsumerfeign;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author madison
 * @description 异常处理
 * @date 2021/10/1 12:18
 */
@Component
public class ProviderClientFallbackFactory implements FallbackFactory<ProviderClient> {
    @Override
    public ProviderClient create(Throwable cause) {
        return str -> "fall back str:" + str + ", error:" + cause;
//        return new ProviderClient() {
//            @Override
//            public String echo(String str) {
//                return "fall back str:" + str + ", error:" + cause;
//            }
//        };
    }
}
