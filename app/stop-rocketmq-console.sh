NAME='rocketmq-console-ng-2.0.0.jar'
ID=`ps -ef | grep "$NAME" | grep -v "grep" | awk '{print $2}'`  #注意此shell脚本的名称，避免自杀
for id in $ID
do
        kill -9 $id
        echo "killed $NAME pid is $id"
done
