package com.zs.nacossentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author madison
 * @description
 * @date 2021/3/29 22:24
 */
@RestController
public class ProviderController {

    /**
     * @param str
     * @return
     * @SentinelResource 注解，有以下的属性：
     * value：资源名称，必需项（不能为空）
     * entryType：entry 类型，可选项（默认为 EntryType.OUT）
     * blockHandler / blockHandlerClass: blockHandler 对应处理 BlockException 的函数名称，可选项
     * fallback：fallback 函数名称，可选项，用于在抛出异常的时候提供 fallback 处理逻辑。
     * <p>
     * 注意：注解方式埋点不支持 private 方法。
     * blockHandler / blockHandlerClass: blockHandler 对应处理 BlockException 的函数名称，可选项。
     * blockHandler 函数访问范围需要是 public，返回类型需要与原方法相匹配，参数类型需要和原方法相匹配并且最后加一个额外的参数，
     * 类型为 BlockException。blockHandler 函数默认需要和原方法在同一个类中。
     * 若希望使用其他类的函数，则可以指定 blockHandlerClass 为对应的类的 Class 对象，注意对应的函数必需为 static 函数，否则无法解析。
     * <p>
     * http://127.0.0.1:8830/echo/234
     */
    @GetMapping("echo/{str}")
//    @SentinelResource(value = "echoResource", blockHandler = "blockHandlerException", fallback = "fallbackException")
    @SentinelResource(value = "echoResource", blockHandler = "blockHandlerClass", blockHandlerClass = BlockHandler.class, fallback = "fallbackException")
    public String echo(@PathVariable String str, @RequestParam(required = false) String param) {
        return "Hello Nacos sentinel msg: " + Integer.valueOf(str) + "," + param;
    }

    public String blockHandlerException(String str, String param, BlockException blockException) {
        return "blockHandler:" + str + ",exception:" + blockException.toString();
    }

    public String fallbackException(String str, String param, Throwable throwable) {
        return "fallback:" + str + ",throwable:" + throwable.toString();
    }
}
