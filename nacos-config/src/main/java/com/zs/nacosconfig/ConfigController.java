package com.zs.nacosconfig;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author madison
 * @description
 * @date 2020/10/17 12:11 上午
 */
@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Value("${useLocalCache:test}")
    private String useLocalCache;

    @Value("${foo:foo}")
    private String foo;

    @PostConstruct
    public void init() {
        System.out.printf("[init] useLocalCache : %s , foo : %s%n", useLocalCache, foo);
    }

    @PreDestroy
    public void destroy() {
        System.out.printf("[destroy] useLocalCache : %s , foo : %s%n", useLocalCache, foo);
    }

    /**
     * curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos-config.properties&group=DEFAULT_GROUP&content=useLocalCache=true"
     * curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos-config.yml&group=DEFAULT_GROUP&content=useLocalCache=true"
     * curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos-config.yml&group=DEFAULT_GROUP&content=useLocalCache=ffff"
     * <p>
     * <p>
     * curl http://localhost:8290/config/get
     *
     * @return
     */
    @GetMapping("get")
    public String get() {
        return useLocalCache;
    }

    /**
     * http://localhost:8290/config/foo
     * @return
     */
    @GetMapping("foo")
    public String foo() {
        return foo;
    }
}
