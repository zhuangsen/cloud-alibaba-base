package com.zs.nacossentinelfeign;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author madison
 * @description
 * @date 2021/6/28 16:06
 */
@Component
public class EchoServiceFallbackFactory implements FallbackFactory<ProviderClient> {
    @Override
    public ProviderClient create(Throwable cause) {
        return str -> "echo fallback factory: " + str + ", " + cause.toString();
    }
    //  EchoServiceFallback echoServiceFallback = new EchoServiceFallback();
    //  echoServiceFallback.setThrowable(cause);
    //  return echoServiceFallback;
}
