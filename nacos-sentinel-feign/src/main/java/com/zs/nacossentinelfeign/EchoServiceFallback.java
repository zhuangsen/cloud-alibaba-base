package com.zs.nacossentinelfeign;

/**
 * @author madison
 * @description
 * @date 2021/6/28 15:54
 */
//@Component
@Deprecated
public class EchoServiceFallback implements ProviderClient {
    private Throwable throwable;

    @Override
    public String echo(String str) {
        return "echo fallback: " + str + ", " + throwable.toString();
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
