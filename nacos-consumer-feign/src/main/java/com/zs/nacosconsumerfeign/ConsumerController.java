package com.zs.nacosconsumerfeign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author madison
 * @description
 * @date 2021/3/29 22:19
 */
@RestController
public class ConsumerController {
    @Autowired
    private ProviderClient providerClient;

    /**
     * http://127.0.0.1:8220/echo/234
     * @param str
     * @return
     */
    @GetMapping("echo/{str}")
    public String echo(@PathVariable String str) {
        return providerClient.echo(str);
    }
}
