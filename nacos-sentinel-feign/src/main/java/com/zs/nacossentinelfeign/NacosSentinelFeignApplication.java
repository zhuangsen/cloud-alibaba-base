package com.zs.nacossentinelfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class NacosSentinelFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosSentinelFeignApplication.class, args);
    }
}
