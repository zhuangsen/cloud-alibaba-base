package com.zs.nacossentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * @author madison
 * @description
 * @date 2021/6/28 15:35
 */
public class BlockHandler {
    public static String blockHandlerClass(String str,String param, BlockException blockException) {
        return "blockHandler class:" + str + ",exception:" + blockException.toString();
    }
}
