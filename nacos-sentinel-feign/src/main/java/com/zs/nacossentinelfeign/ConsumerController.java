package com.zs.nacossentinelfeign;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author madison
 * @description
 * @date 2021/3/29 22:24
 */
@RestController
public class ConsumerController {
    @Autowired
    private ProviderClient providerClient;

    @GetMapping("/echo/{str}")
    @SentinelResource(value = "/echo/{str}")
    public String echo(@PathVariable String str) {
        return providerClient.echo(str);
    }
}
