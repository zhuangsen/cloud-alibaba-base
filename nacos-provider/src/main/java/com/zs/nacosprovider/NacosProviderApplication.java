package com.zs.nacosprovider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@EnableDiscoveryClient
public class NacosProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosProviderApplication.class, args);
    }

    @Value("${server.port}")
    private int port;

    @RestController
    class EchoController {
        @GetMapping(value = "echo/{string}")
        public String echo(@PathVariable String string) {
            Integer.valueOf(string);
            return "Hello Nacos Discovery: " + port + ", msg: " + string;
        }
    }
}
