package com.zs.nacosconsumerfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author madison
 * @description
 * @date 2021/3/29 22:19
 */
@FeignClient(value = "nacos-provider", fallbackFactory = ProviderClientFallbackFactory.class)
public interface ProviderClient {
    @GetMapping("echo/{str}")
    String echo(@PathVariable String str);
}
